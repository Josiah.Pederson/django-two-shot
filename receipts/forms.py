from django import forms
from receipts.models import Receipt, Account, ExpenseCategory



class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account"
        ]

    def __init__(self, owner, *args, **kwargs):
        super().__init__(*args, **kwargs)
        account = forms.ModelChoiceField(queryset=Account.objects.filter(owner=owner))
        category = forms.ModelChoiceField(queryset=ExpenseCategory.objects.filter(owner=owner))
        self.fields["account"] = account
        self.fields["category"] = category
            
