from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ReceiptUpdateView,
    ReceiptDeleteView,
    CategoryListView,
    CategoryCreateView,
    CategoryUpdateView,
    CategoryDeleteView,
    AccountListView,
    AccountCreateView,
    AccountUpdateView,
    AccountDeleteView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="receipts_list_url"),
    path("create/", ReceiptCreateView.as_view(), name="receipts_new_url"),
    path("update/<int:pk>/", ReceiptUpdateView.as_view(), name="receipts_edit_url"),
    path("delete/<int:pk>/", ReceiptDeleteView.as_view(), name="receipts_delete_url"),
    path("categories/", CategoryListView.as_view(), name="category_list_url"),
    path("categories/create/", CategoryCreateView.as_view(), name="category_new_url"),
    path("categories/update/<int:pk>/", CategoryUpdateView.as_view(), name="category_edit_url"),
    path("categories/delete/<int:pk>/", CategoryDeleteView.as_view(), name="category_delete_url"),
    path("accounts/", AccountListView.as_view(), name="account_list_url"),
    path("accounts/create/", AccountCreateView.as_view(), name="account_new_url"),
    path("accounts/update/<int:pk>/", AccountUpdateView.as_view(), name="account_edit_url"),
    path("accounts/delete/<int:pk>/", AccountDeleteView.as_view(), name="account_delete_url"),
]
