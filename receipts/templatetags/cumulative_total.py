# import something
from django import template

register = template.Library()


def cumulative_total(all_receipts):
    if len(all_receipts) > 0:
        added_total = 0
        for receipt in all_receipts:
            added_total += receipt.total
        return added_total
    return None

register.filter("cumulative_total", cumulative_total)