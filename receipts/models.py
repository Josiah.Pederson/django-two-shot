from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# import datetime
# Create your models here.

class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(
        decimal_places=3,
        max_digits=10,
        validators= [
            MinValueValidator(0)
        ],
        )
    tax = models.DecimalField(
        decimal_places=3,
        max_digits=10,
        validators= [
            MaxValueValidator(1.0),
            MinValueValidator(0.0)
        ],
    )
    date = models.DateField(auto_now=False)
    category = models.ForeignKey("ExpenseCategory", related_name="receipts", on_delete=models.CASCADE)
    account = models.ForeignKey("Account", related_name="receipts", on_delete=models.CASCADE)
    owner = models.ForeignKey(USER_MODEL, related_name="receipts", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "Receipt: " + str(self.vendor)


class Account(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(USER_MODEL, related_name="accounts", on_delete=models.CASCADE, null=True)
    number = models.CharField(max_length=20)

    def __str__(self):
        return "Account: " + str(self.name)


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(USER_MODEL, related_name="categories", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "Category: " + str(self.name)
